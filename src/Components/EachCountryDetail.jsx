import React, { Component } from 'react'
import * as CountryApi from './Api'
import { Link } from 'react-router-dom'
import { Box, Image, List, ListItem, Button, Text } from "@chakra-ui/react"
import { BsArrowLeft} from 'react-icons/bs'

export default class EachCountryDetail extends Component {
   constructor(props) {
     super(props)
   
     this.state = {
        country: null,
        isLoading: true,
        hasError: false
     }
   }
   async componentDidMount(){
    const {code} = this.props.match.params;
    try{
        const country = await CountryApi.getCountryByCode(code)
        this.setState({
            country: country,
            isLoading: false,
            hasError: false
        })
   
       //console.log(country.data[0].name.nativeName[code.toLowerCase()].official, code.toLowerCase())
    }
   
    catch(err){
        this.setState({
            country: null,
            isLoading: false,
            hasError: `Error occured fetching the data by ${code}`
        })
    }
   
   }

   async componentDidUpdate(prevProps){
    if(prevProps.match.params.code !== this.props.match.params.code){
      const {code} = this.props.match.params;
      try{
        const country = await CountryApi.getCountryByCode(code)
        this.setState({
            country: country,
            isLoading: false,
            hasError: false
        })
    }
   
    catch(err){
        this.setState({
            country: null,
            isLoading: false,
            hasError: `Error occured fetching the data by ${code}`
        })
    }
    }
   }
  render() {
    if(this.state.hasError){
        return (
          <Box fontSize='3xl'as='b' >
            {this.state.hasError}
          </Box>
        )
      }
  
  
      if(this.state.isLoading){
        return( <Box fontSize='3xl'as='b' >
                   Loading...
              </Box>
      )}
    

        return (
            <>
            <Link to={'/'}><Button width='100px' leftIcon={<BsArrowLeft/>} my={18} ml={20} >Back</Button></Link>

            <Box ml={20} mt={18} display='flex' >
              
             <Box width='40%' >
                <Image src={this.state.country.data[0].flags.png} alt='country-pic'  h='300px' w='450px' />
             </Box>
             <Box width='60%' >
                <Box display='flex'>
                      <Box ml={20} mt={10} >

                           <Box fontSize='2xl' as='b' >{this.state.country.data[0].name.common}</Box>
                           <List marginBottom={20}  >
                             {this.state.country.data[0].name.nativeName[this.props.match.params.code.toLowerCase()]
                             && <ListItem mt={5} mb={2} ><Text>Native Name: {this.state.country.data[0].name.nativeName[this.props.match.params.code.toLowerCase()].official}</Text></ListItem>}
                              <ListItem mb={2} ><Text>Population: {this.state.country.data[0].population}</Text></ListItem>
                              <ListItem mb={2} ><Text>Region: {this.state.country.data[0].region}</Text></ListItem>
                              <ListItem mb={2} ><Text>Sub region: {this.state.country.data[0].subregion}</Text></ListItem>
                              <ListItem mb={2} ><Text>Capital: {this.state.country.data[0].capital[0]}</Text></ListItem>
                           </List>
                      </Box>

                      <Box ml={20} mt={20} >
                          <List>
                          <ListItem mb={2} ><Text fontSize='lg'>Top Level Domain: {this.state.country.data[0].tld[0]}</Text></ListItem>
                          <ListItem mb={2} ><Text>Currencies: {  this.state.country.data[0].currencies[Object.keys(this.state.country.data[0].currencies)[0]].name}</Text></ListItem>
                          <ListItem mb={2} ><Text>Languages: {Object.values(this.state.country.data[0].languages).join(', ')}</Text></ListItem>
                          </List>
                      </Box>
                </Box>

                <Box ml={20} >
                    <Text fontSize='xlg' >Borders countries:
                  {(this.state.country.data[0].borders || []).map(border => <Link to={border}><Button mx={5} mb={5} >{border}</Button></Link>)}
                  </Text>
                </Box>
             </Box>

            </Box>

            </>
          )


    
  }
}
