import React, { Component } from 'react'
import * as CountryApi from './Api'
import { Box, Input, Select, Image, List, ListItem,Text, InputGroup, InputLeftElement } from "@chakra-ui/react"
import { BsSearch} from 'react-icons/bs'

 import { Link } from 'react-router-dom'

export default class Content extends Component {


    constructor(props) {
      super(props)
    
      this.state = {
        countries: [],
        isLoading: true,
        hasError: false,
        searchText: '',
        region: 'all'
      }
    }



    componentDidMount(){
   CountryApi.getAllCountries().then((countries) => {
    this.setState({
      countries,
      isLoading: false,
      hasError: false
    })
   }).catch(err => {
    this.setState({
      countries: [],
      isLoading: false,
      hasError: 'Failed to load the countries'
    })
   })
    }

    handleInput = (event) => {
      this.setState({
         searchText: event.target.value
      })
    }

    handleDropDown = (event) => {
      this.setState({
        region: event.target.value
      })
    }


  render() {


    if(this.state.hasError){
      return (
        <Box fontSize='3xl'as='b' >
          {this.state.hasError}
        </Box>
      )
    }


    if(this.state.isLoading){
      return( <Box fontSize='3xl' as='b' >
                 Loading...
            </Box>
    )}


    const searchResults = this.state.searchText === ''
                           ? this.state.countries 
                           : this.state.countries.filter(eachCountryData => {
                             const searchTextLength = this.state.searchText.length
                            return eachCountryData.name.common.slice(0, searchTextLength).toLowerCase() === this.state.searchText.toLowerCase()
                           }) 

     
    const filteredResult = this.state.region === 'all'
                           ? searchResults
                           : searchResults.filter(eachCountryData => {
                            return eachCountryData.region === this.state.region
                           })

    return (
      <>

          



        <Box display="flex" justifyContent="space-between" h='60px' alignItems='center'>
            <InputGroup>
            <InputLeftElement pointerEvents='none' marginLeft='80px' children={<BsSearch color='gray.300' />}/>
            <Input type='text' size='md' w='400px'  placeholder='Search for a Country' onChange={this.handleInput} ml={20}/>
            </InputGroup>
            <Select  onChange={this.handleDropDown} mr={20} placeholder='Filter by Region'  value='all' w='200px'>
                  <option value='Africa'>Africa</option>
                  <option value='Americas'>Americas</option>
                  <option value='Asia'>Asia</option>
                  <option value='Europe'>Europe</option>
                  <option value='Oceania'>Oceania</option>
           </Select>
        </Box>
        

        <Box display='flex' flexWrap='wrap' ml={38}>{filteredResult.map(eachCountryData  => {
              return( 
               <Link  to={`/${eachCountryData.cca3}`} key={eachCountryData.cca3}> 
                <Box m={9}  border='1px' borderColor='gray.200' borderRadius='6px' >
                <Image src={eachCountryData.flags.png} alt='country-pic' h='130px' w='230px' mb={6} borderTopRadius='6px' />
                        <List spacing={1} ml={4} >
                            <ListItem fontSize='1xl' as='b' >{eachCountryData.name.common}</ListItem>
                            <ListItem> <Text>Population: {eachCountryData.population}</Text></ListItem>
                            <ListItem><Text>Region: {eachCountryData.region}</Text></ListItem>
                            <ListItem mb={10}><Text>Capital: {eachCountryData.capital}</Text></ListItem>
                        </List>
                </Box>
                </Link>
             
              )
            })}
        </Box>

      </>
    )
  }
}