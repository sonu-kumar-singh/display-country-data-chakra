import React, { Component } from 'react'
import { Box, Button } from "@chakra-ui/react"
export default class Header extends Component {

  render() {
    return (
        <Box display="flex" justifyContent="space-between" alignItems="center" h="50px"  borderBottom='2px' borderColor='gray.200'>
                <Box ml={20} fontSize='1xl'as='b' >Where in the world?</Box>
                <Button  mr={20} fontSize='sm'>Dark Mode</Button>
        </Box>
    )
  }
}