import React, { Component } from 'react'
import {
  ChakraProvider,
} from '@chakra-ui/react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'
// import { ColorModeSwitcher } from './ColorModeSwitcher';
import Content from './Components/Content';
import Header from './Components/Header';
import EachCountryDetail from './Components/EachCountryDetail';

export default class App extends Component {
  render() {
    return (
      <ChakraProvider>
        <Header />
        <Router>
          <Switch>
            <Route exact path="/"component={Content}/>
            <Route path="/:code" component={ EachCountryDetail } />
          </Switch>
        </Router>
          
      </ChakraProvider>
    )
  }
}
